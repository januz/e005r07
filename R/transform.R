#' Label data
#'
#' Applies value and variable labels to the data frame containing the raw data.
#'
#' @param data tibble. The data frame with the raw data.
#'
#' @return tibble. Data frame with labels applied.
#' @export
label_data <- function(data) {
  # scales used by several variables
  scale_yes_no <- c(
    "Yes" = 1,
    "No" = 2
  )
  scale_likert <- c(
    "Eat more" = 1,
    "Eat the same amount" = 2,
    "Eat a little less" = 3,
    "Eat a lot less" = 4,
    "Stop eating" = 5,
    "Already do not eat" = 6
  )

  data %>%
    # add variable labels
    sjlabelled::var_labels(
      Age_24_Under = "Age group",
      Q6_Cover_Messages = "Cover messages (study 2)",
      Q83_PamphletMessage = "Message",
      Q84_GenIntentToChange = "Intent to change",
      Q85_FutureBeefPork = "Beef/Pork",
      Q86_FutureChicken = "Poultry",
      Q87_FutureFish = "Fish",
      Q88_FutureEggs = "Eggs",
      Q89_FutureDairy = "Dairy",
      Q90_IsOrderedVegRecipe = "Recipe book",
      Q92_AgeGroup = "Age",
      Q99_SelfCat = "Dietary identity"
    ) %>%

    # add value labels
    sjlabelled::val_labels(
      Age_24_Under = c(
        "Age > 24" = 0,
        "Age < 25" = 1
      ),
      Q6_Cover_Messages = c(
        "Animal message" = 1,
        "Health message" = 2,
        "Animal, health, & environment message" = 3,
        "Animal and health message" = 4
      ),
      Q83_PamphletMessage = c(
        "Purity" = 1,
        "Cruelty" = 2,
        "Environment" = 3
      ),
      Q92_AgeGroup = c(
        "Under 18 years" = 1,
        "18 to 24 years" = 2,
        "25 to 34 years" = 3,
        "35 to 44 years" = 4,
        "45 to 54 years" = 5,
        "55 to 64 years" = 6,
        "Age 65 or older" = 7,
        "Prefer not to answer" = 8
      ),
      Q99_SelfCat = c(
        "omnivore" = 1,
        "meat reducer" = 2,
        "semi-vegetarian" = 3,
        "vegetarian" = 4,
        "vegan" = 5
      ),
      Q84_GenIntentToChange = !!scale_yes_no,
      Q90_IsOrderedVegRecipe = !!scale_yes_no,
      Q85_FutureBeefPork = !!scale_likert,
      Q86_FutureChicken = !!scale_likert,
      Q87_FutureFish = !!scale_likert,
      Q88_FutureEggs = !!scale_likert,
      Q89_FutureDairy = !!scale_likert
    )
}

#' Compute days of suffering spared
#'
#' Helper function to compute "days of suffering spared" (DOSS) for the
#'   different variables assessing participant's intention to change their
#'   consumption of specific animal products.
#'
#' @param data tibble. A data frame.
#' @param var_in character. Name of a variable assessing the intention to change
#'   the consumption of a specific animal product.
#' @param var_out character. Name for the computed DOSS variable.
#' @param dos numeric. Days of suffering experienced by animals per year, as
#'   reported in the original report.
#'
#' @return tibble. The input data frame with an added column for the computed
#'   DOSS variable.
#' @export
compute_doss <- function(data, var_in, var_out, dos) {
  var_out = rlang::enquo(var_out)

  data %>%
    dplyr::mutate(
      !!var_out := dplyr::case_when(
        .data[[var_in]] == 1 ~ -.2 * dos,
        .data[[var_in]] == 2 ~ 0,
        .data[[var_in]] == 3 ~ .1 * dos,
        .data[[var_in]] == 4 ~ .35 * dos,
        .data[[var_in]] == 5 ~ dos,
        .data[[var_in]] == 6 ~ 0
      )
    )
}

#' Transform data
#'
#' Transforms data for analysis by excluding participants who didn't complete
#'   the relevant part of the survey and computing days of suffering spared.
#'
#' @param data tibble. A data frame.
#'
#' @return tibble. The transformed data frame.
#' @export
transform_data <- function(data) {
  data_with_labels <- data
  data %>%
    # convert to factor variables
    sjlabelled::as_factor() %>%

    # exclude participants who didn't complete the relevant part of the survey
    dplyr::filter(!is.na(Q83_PamphletMessage)) %>%
    sjlabelled::copy_labels(data_with_labels) %>% # re-apply labels

    # compute days of suffering spared (DOSS)
    compute_doss("Q85_FutureBeefPork", "DOSS_BeefPork", 113) %>%
    compute_doss("Q86_FutureChicken", "DOSS_Poultry", 1220) %>%
    compute_doss("Q87_FutureFish", "DOSS_Fish", 1500) %>%
    compute_doss("Q88_FutureEggs", "DOSS_Eggs", 365) %>%
    compute_doss("Q89_FutureDairy", "DOSS_Dairy", 12) %>%
    dplyr::mutate(
      DOSS_Overall = rowSums(dplyr::select(., dplyr::matches("DOSS_")))
    )
}

#' Filter out older participants
#'
#' Helper functions to filter out all participants older than 24 years of age.
#'   Used to prepare a reduced data frame to reproduce the subgroup analyses
#'   reported in the original report.
#'
#' @param data tibble. A data frame.
#'
#' @return tibble. The data frame containing only participants < 25 years of age
#' @export
filter_under_25 <- function(data) {
  data_with_labels <- data
  data %>%
    dplyr::filter(Age_24_Under == 1) %>%
    sjlabelled::copy_labels(data_with_labels)
}

#' Prepare data for reanalysis
#'
#' Transforms data frame for the reanalysis by excluding participants who did
#'   not complete all questions and reordering factor levels to set the cruelty
#'   message as the reference level for dummy variables in regression models.
#'
#' @param data tibble. A data frame.
#'
#' @return tibble. The data frame transformed for the reanalysis.
#' @export
prep_reanalysis <- function(data) {
  data_with_labels <- data
  data %>%
    # exclude participant who did not indicate their age
    dplyr::filter(!is.na(Age_24_Under)) %>%
    sjlabelled::copy_labels(data_with_labels) %>% # re-apply labels

    dplyr::mutate(
      # use value labels for message variable
      Q83_PamphletMessage = sjlabelled::as_label(.$Q83_PamphletMessage),

      # reorder factor levels so that comparisons are expressed with "Cruelty"
      # message
      Q83_PamphletMessage = forcats::fct_relevel(
        Q83_PamphletMessage, "Cruelty", "Environment"
      ),

      # reverse factor levels order so that odds (ratios) are expressed for
      # "Yes"
      Q84_GenIntentToChange = forcats::fct_relevel(
        Q84_GenIntentToChange, "2"
      ),
      Q84_GenIntentToChange = forcats::fct_recode(
        Q84_GenIntentToChange, "0" = "2"
      ),
      Q84_GenIntentToChange = sjlabelled::set_labels(
        Q84_GenIntentToChange,
        labels = c("No" = 0, "Yes" = 1)
      ),

      Q90_IsOrderedVegRecipe = forcats::fct_relevel(
        Q90_IsOrderedVegRecipe, "2"
      ),
      Q90_IsOrderedVegRecipe = forcats::fct_recode(
        Q90_IsOrderedVegRecipe, "0" = "2"
      ),
      Q90_IsOrderedVegRecipe = sjlabelled::set_labels(
        Q90_IsOrderedVegRecipe,
        labels = c("No" = 0, "Yes" = 1)
      )
    )
}