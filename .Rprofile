# start `renv`
source("renv/activate.R")

# load package
devtools::load_all()

#' Run analysis
#'
#' Convenience function that executes `devtools::load_all()` (optionally
#'   `devtools::document()`) and then runs the analysis pipeline using
#'   `reproduce_analysis()`.
#'
#' @param document run roxygen (default `FALSE`).
#'
#' @return NULL
run_analysis <- function(document = FALSE, rerun = FALSE) {
  if (document) {
    devtools::document()
  } else {
    devtools::load_all()
  }

  if (rerun) {
    reproduce_analysis(rerun = TRUE)
    file.copy(".drake", pkgenv$source_path, recursive = T)
    unlink(".drake", recursive = TRUE)
    file.symlink(paste0(pkgenv$source_path, "/.drake"), ".")
    unlink(".drake/.gitignore")
  } else {
    copy_analysis(symlink = TRUE)
    reproduce_analysis()
  }
}
